package com.bank.product.repository;

import com.bank.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    //@Query("SELECT u FROM Product u WHERE u.productCode = :productCode")
    Optional<Product> findProductByProductCode(String productCode);
}