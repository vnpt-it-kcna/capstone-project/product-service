package com.bank.product.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "product_event")
public class ProductEvent {
    @Id
    @Column(name = "event_id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "product_id")
    private UUID productId;

    @Column(name = "product_code", nullable = false)
    private String productCode;

    @Column(name = "product_limit_type", nullable = false)
    private String productLimitType;

    @Column(name = "product_limit_amount")
    private BigDecimal productLimitAmount;

    @Column(name = "product_last_updated_at")
    private Instant productLastUpdatedAt;

    @Column(name = "event_created_at")
    private Instant eventCreatedAt;

    public Instant getEventCreatedAt() {
        return eventCreatedAt;
    }

    public void setEventCreatedAt(Instant eventCreatedAt) {
        this.eventCreatedAt = eventCreatedAt;
    }

    public Instant getProductLastUpdatedAt() {
        return productLastUpdatedAt;
    }

    public void setProductLastUpdatedAt(Instant productLastUpdatedAt) {
        this.productLastUpdatedAt = productLastUpdatedAt;
    }

    public BigDecimal getProductLimitAmount() {
        return productLimitAmount;
    }

    public void setProductLimitAmount(BigDecimal productLimitAmount) {
        this.productLimitAmount = productLimitAmount;
    }

    public String getProductLimitType() {
        return productLimitType;
    }

    public void setProductLimitType(String productLimitType) {
        this.productLimitType = productLimitType;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public UUID getProductId() {
        return productId;
    }

    public void setProductId(UUID productId) {
        this.productId = productId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}